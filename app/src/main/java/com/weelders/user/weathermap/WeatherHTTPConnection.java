package com.weelders.user.weathermap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * This project is make by Rodolphe on 03/09/2018
 */
public class WeatherHTTPConnection
    {
    private final static String URL_BASE = "http://api.openweathermap.org/data/2.5/";
    private String forecastInterval;
    private String cityName;
    private final static String PARAMETERS = "&units=metric&lang=fr&APPID=";
    private final static String API_KEY = "0ad848e4d517d1b19037edcdf6c24bd9";

    private String mURL;


    private final static String URL_BASE_ICON = "http://openweathermap.org/img/w/";
    private String iconID;
    private final static String ICON_FORMAT = ".png";
    private String mIconUrl;

    public String getJson()
        {
        Log.i("URL DATA",getmURL());//Log JSON URL
        return getData(getmURL());//return JSON URL
        }

    public Bitmap getBitmap()
        {
        byte b[] = getIcon(getmIconUrl());//Get ByteArray to URL
        Bitmap img = BitmapFactory.decodeByteArray(b,0,b.length);//Transform byteArray to Bitmap picture
        return img;//Return Bitmap picture
        }

    public WeatherHTTPConnection(String forecastInterval, String cityName)
        {
        this.setForecastInterval(forecastInterval);
        this.setCityName(cityName);
        this.setmURL(URL_BASE + getForecastInterval() + getCityName() + PARAMETERS + API_KEY);
        }

    public WeatherHTTPConnection(String iconID)
        {
        this.setIconID(iconID);
        this.setmIconUrl(URL_BASE_ICON + getIconID() + ICON_FORMAT);

        }


    public static String getData(String url)
        {
        HttpURLConnection con = null;
        InputStream is = null;

        try
            {
            con = (HttpURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(false);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();
            return buffer.toString();
            }
        catch(Throwable t)
            {
            t.printStackTrace();
            }
        finally
            {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
            }

        return "";

        }

    public static byte[] getIcon(String iconUrl)
        {

        HttpURLConnection con = null;
        InputStream is = null;

        try
            {
            con = (HttpURLConnection) ( new URL(iconUrl)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(false);
            con.connect();

            Log.i("URLicon",iconUrl);//Log Icon URL

            // Let's read the response
            ByteArrayOutputStream buffer=new ByteArrayOutputStream();
            is = con.getInputStream();
            byte[] data = new byte[1024];
            int nRead;
            while ((nRead=is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
            }

            buffer.flush();
            //Get Picture in byteArray
            return buffer.toByteArray();

            }
        catch(Throwable t)
            {
            t.printStackTrace();
            }
        finally
            {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
            }

        return null;

        }



    public String getCityName()
        {
        return cityName;
        }

    public void setCityName(String cityName)
        {
        this.cityName = cityName;
        }


    public String getForecastInterval()
        {
        return forecastInterval;
        }

    public void setForecastInterval(String forecastInterval)
        {
        this.forecastInterval = forecastInterval;
        }

    public String getmURL()
        {
        return mURL;
        }

    public void setmURL(String mURL)
        {
        this.mURL = mURL;
        }


    public String getIconID()
        {
        return iconID;
        }

    public void setIconID(String iconID)
        {
        this.iconID = iconID;
        }

    public String getmIconUrl()
        {
        return mIconUrl;
        }

    public void setmIconUrl(String mIconUrl)
        {
        this.mIconUrl = mIconUrl;
        }
    }
