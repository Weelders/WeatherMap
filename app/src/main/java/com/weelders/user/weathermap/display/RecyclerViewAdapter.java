package com.weelders.user.weathermap.display;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weelders.user.weathermap.IconAsyncTask;
import com.weelders.user.weathermap.OnTaskCompletedIcon;
import com.weelders.user.weathermap.R;
import com.weelders.user.weathermap.model.WeatherMain;



import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This project is make by Rodolphe on 04/09/2018
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder>
    {
    private WeatherMain mWeatherMain;
    DecimalFormat formater = new DecimalFormat("###,###.#");


    public RecyclerViewAdapter(WeatherMain weatherMain)
        {
        this.mWeatherMain = weatherMain;



        }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row,parent,false);
        ViewHolder mViewHolder = new ViewHolder(itemView);
        return mViewHolder;
        }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
        {


        //Recuperation of "DT" on JSON and make Date format
        Date date = new Date(mWeatherMain.getList().get(position).getDt()); //DT JSON in second * 1000 for milisecond Date format in "List" object
        SimpleDateFormat format = new SimpleDateFormat("EEEE dd MMM '--' kk:mm");
        holder.mtv_time.setText(format.format(date).toUpperCase());



        //Recuperation temp
        //-infini -> 18 Blue
        //25 -> +infini RED
        //Green default
        double temp = mWeatherMain.getList().get(position).getMain().getTemp();
        holder.mtv_temp.setText(formater.format(temp)+"°C");
        holder.mtv_temp.setTextColor(TempColorMaker(temp));

        //Humidity
        holder.mtv_humidity.setText(String.valueOf(mWeatherMain.getList().get(position).getMain().getHumidity()));

        //Description
        holder.mtv_description.setText(mWeatherMain.getList().get(position).getWeather().get(0).getDescription().toUpperCase());

        //Get icon by HTTP request on AsyncTask
        IconAsyncTask iconAsyncTask = new IconAsyncTask( new OnTaskCompletedIcon<Bitmap>()
            {
            @Override
            public void update(Bitmap bitmap)
                {
                holder.miv_icon.setImageBitmap(bitmap);
                }
            },mWeatherMain.getList().get(position).getWeather().get(0).getIcon());

        iconAsyncTask.execute();

        }

    private int TempColorMaker(double temp)
        {
        int textColor;

        if (temp > 25d)
            {
            textColor = Color.rgb(200, 00, 00); //RedColor
            }
        else if (temp < 18d)
            {
            textColor = Color.rgb(45, 125, 255); //BlueColor
            }
        else
            {
            textColor = Color.rgb(91, 180, 82); //GreenColor
            }

        return textColor;
        }


    @Override
    public int getItemCount()
        {
        return mWeatherMain.getList().size();
        }

    }

