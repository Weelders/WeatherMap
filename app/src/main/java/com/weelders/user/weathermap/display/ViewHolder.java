package com.weelders.user.weathermap.display;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weelders.user.weathermap.R;
/**
 * This project is make by Rodolphe on 04/09/2018
 */
public class ViewHolder extends RecyclerView.ViewHolder
    {
    public ImageView miv_icon;
    public TextView mtv_temp,mtv_humidity,mtv_description,mtv_time;


    public ViewHolder(View view)
        {
        super(view);
        mtv_time = (TextView) view.findViewById(R.id.tv_time);
        mtv_temp = (TextView) view.findViewById(R.id.tv_temp);
        mtv_humidity = (TextView) view.findViewById(R.id.tv_humidity);
        mtv_description = (TextView) view.findViewById(R.id.tv_description);
        miv_icon = (ImageView) view.findViewById(R.id.iv_icon);
        }
    }