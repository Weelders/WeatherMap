package com.weelders.user.weathermap;

import com.weelders.user.weathermap.model.WeatherMain;

/**
 * This project is make by Rodolphe on 03/09/2018
 */
public interface OnTaskCompleted<WeatherMain>
    {
    public void updateUI(WeatherMain weatherMain);
    }
