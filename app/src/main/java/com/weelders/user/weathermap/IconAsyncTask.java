package com.weelders.user.weathermap;


import android.graphics.Bitmap;
import android.os.AsyncTask;


/**
 * This project is make by Rodolphe on 04/09/2018
 */
public class IconAsyncTask extends AsyncTask<String,Void,Bitmap>
    {
    private OnTaskCompletedIcon mCallBack;
    private String mIconID;


    public IconAsyncTask(OnTaskCompletedIcon callBack, String iconID)
        {
        mCallBack = callBack;
        mIconID = iconID;
        }


    @Override
    protected Bitmap doInBackground(String... strings)
        {
        return new WeatherHTTPConnection(mIconID).getBitmap();
        }

    @Override
    protected void onPostExecute(Bitmap b)
        {
        super.onPostExecute(b);

        mCallBack.update(b);
        }
    }
