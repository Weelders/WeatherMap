package com.weelders.user.weathermap;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.weelders.user.weathermap.display.RecyclerViewAdapter;
import com.weelders.user.weathermap.model.WeatherMain;

public class MainActivity extends AppCompatActivity
    {

    private RecyclerView mrv_view;
    private Button mbtn_newsearch;
    private RecyclerViewAdapter mAdapter;
    private String mCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mrv_view = findViewById(R.id.rv_view);
        mbtn_newsearch = findViewById(R.id.btn_newsearch);

        //Call Method for pop-up window
        locationFinderAlert();


        mbtn_newsearch.setOnClickListener(new View.OnClickListener()
            {
            @Override
            public void onClick(View v)
                {
                //Call Method for pop-up window
                locationFinderAlert();
                }
            });

        }

    private boolean isConnected()
        {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
        }

    private void locationFinderAlert()
        {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false); //Can't use return button
        final EditText met_cityname = new EditText(MainActivity.this); //Create editText on alert popup
        met_cityname.setHint("Enter City Name");
        builder.setView(met_cityname); //Set editText like view on popup


        //Negative button = left button
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {

            public void onClick(DialogInterface dialog, int which)
                {
                dialog.cancel(); //Close popup
                }
            });

        builder.setPositiveButton("Search", new DialogInterface.OnClickListener()
            {
            @Override
            public void onClick(DialogInterface dialog, int which)
                {

                mCityName = String.valueOf(met_cityname.getText());//Get City name

                if (met_cityname.getText().toString().isEmpty())

                    {
                    Toast.makeText(getApplicationContext(), "A City Name Is Requiered", Toast.LENGTH_LONG).show(); //Toast for city name empty, crash patch
                    }
                else
                    {
                    GsonWeather gsonWeather = new GsonWeather(getApplicationContext(), new OnTaskCompleted<WeatherMain>()
                        {
                        @Override
                        public void updateUI(WeatherMain weatherMain)
                            {
                            //Connection test
                            if (!isConnected())
                                {
                                Toast.makeText(getApplicationContext(), "Pas de connection !!", Toast.LENGTH_LONG).show();
                                return;
                                }

                            mAdapter = new RecyclerViewAdapter(weatherMain);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            mrv_view.setLayoutManager(mLayoutManager);
                            mrv_view.setItemAnimator(new DefaultItemAnimator());
                            mrv_view.setAdapter(mAdapter);
                            }

                        }, mCityName);

                    gsonWeather.execute();
                    }
                }
            });
        AlertDialog alert = builder.create();
        alert.show();
        }
    }

