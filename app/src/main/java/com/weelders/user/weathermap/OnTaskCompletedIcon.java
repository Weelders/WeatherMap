package com.weelders.user.weathermap;

/**
 * This project is make by Rodolphe on 04/09/2018
 */
public interface OnTaskCompletedIcon<Bitmap>
    {
    public void update(Bitmap bitmap);
    }
