package com.weelders.user.weathermap;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.weelders.user.weathermap.model.WeatherMain;

/**
 * This project is make by Rodolphe on 03/09/2018
 */
public class GsonWeather extends AsyncTask<String,Void,String>
    {
    private String mCityName;
    private OnTaskCompleted mCallBack;
    private Context mContext;


    public GsonWeather(Context context,OnTaskCompleted callBack,String cityname)
        {
        mContext = context;
        mCallBack = callBack;
        mCityName = cityname;
        }

    @Override
    protected void onPreExecute()
        {
        super.onPreExecute();
        }

    @Override
    protected String doInBackground(String... strings)
        {
        //HTTP request to get JSON
        return new WeatherHTTPConnection("forecast?q=",mCityName).getJson();
        }

    @Override
    protected void onPostExecute(String s)
        {
        super.onPostExecute(s);
        Log.i("Json",s);//Log JSON String

        Gson gson = new Gson();
        WeatherMain weatherMain = gson.fromJson(s,WeatherMain.class);//JSON to WeatherMain Object
        mCallBack.updateUI(weatherMain); //Send weatherMainObject by Interface to the mainActivity
        Log.i("Nom de ville",weatherMain.getCity().getName());//Log city name
        }
    }
