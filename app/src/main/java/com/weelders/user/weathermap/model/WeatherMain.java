
package com.weelders.user.weathermap.model;




public class WeatherMain {

    private String cod;

    private Double message;

    private Integer cnt;

    private java.util.List<List> list;

    private City city;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }



    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

public java.util.List<List> getList()
    {
    return list;
    }

public void setList(java.util.List<List> list)
    {
    this.list = list;
    }
}
